<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> Swipe Brick Breaker Neural Net </title>
    <script src="js/processing.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
</head>
<body>
    <div id = "nav-bar">
        <div id = "nav-bar-title">
            Swipe Brick Breaker Neural Net
        </div>
        <a href="../" id = "nav-bar-process">See The Game</a>
    </div>

    <div id = "document-wrapper">
        <h1> The Neural Net </h1>
        <h2> What Is This? </h2>

        I made a neural net to play Swipe Brick Breaker. If you’re not familiar with the game you can find it <a class = "link" href="https://play.google.com/store/apps/details?id=com.Monthly23.SwipeBrickBreaker&hl=en_US" target="_blank">here</a>. I used the Processing.js library to make the game itself. You can see the project on GitLab <a class = "link" href="https://gitlab.com/mljustinli/swipe-block-neural-net" target="_blank">here</a>.

        <img class = "image image-portrait" src="../img/my-version.png" alt="My Version">
        <div class = "image-description"> My recreation of the game in Processing. </div>

        Originally, I thought making a neural net would be too hard so I wanted to create an “AI” that looked at every angle with some small angle increment to find the best shot. The best shot would be defined as “hitting the most number of blocks.” However, I realized that since there are multiple balls, it’s hard to say which blocks will be hit by which balls since blocks can disappear once they reach 0. Also, I would have to deal with taking the ball diameter into account and it would be a mess.

        <h2> The Solution </h2>
        So! I decided to create a neural net that would calculate the angle instead. This might seem harder but I can just let the neural net play with a replica of the game.

        I got a lot of inspiration from Code Bullet on YouTube, especially his <a class = "link" href="https://www.youtube.com/watch?v=SO7FFteErWs" target="_blank">Hill Climb Racing video</a>. In the video, he uses NEAT (NeuroEvolution of Augmenting Topologies). Essentially, he allows a population of random neural nets to play the game, breeds the best performers, and lets them play the game again. Over time, the neural nets will hopefully improve and beat the game.

        Likewise, we’ll create a population of neural nets to play the game until they all die, breed the ones that got the highest score, and breed them to create a new population. The goal is 100!

        <img class = "image image-portrait" src="../img/diagram/neural-net-diagram.png" alt="Neural Net Diagram">
        <div class = "image-description">
            The neural net is just a couple of arrays of weights. Here the input nodes would be an array of weights. The hidden nodes would have an array of weights as well, and so on.
            <br><br>
            For each hidden node, the sum of each input node weight is multiplied by the input at each input node and summed together. Then it’s run through an activation function. This value is then an input for the output layer.
        </div>

        Our neural net will have an input, hidden, and output layer. The output layer is a single angle, so our output layer is a single node.

        <img class = "image image-portrait" src="../img/neural-net-1.png" alt="Our Neural Net">
        <div class = "image-description">
            Our neural net. The input layer is the left most column of nodes, the hidden layer is in the middle, and the output layer is the single node on the right.
            <br><br>
            There are 48 possible spaces where a block can be so our neural net has 48 block input nodes and 1 ball position input node at the bottom.
        </div>



        Our game plan is

        <ul>
            <li>Create the game.</li>
            <li>Create the population of neural nets.</li>
            <li>Let the first batch of neural nets play the game until they all die.
                <ul>
                    <li>I originally had a rule where if the neural net shoots and hits no blocks then it dies. I thought this would help force the neural nets to learn that hitting blocks is a good thing.</li>
                    <li>However, I found that after training this would kill neural nets if they were in the late game, so I changed the rule so that neural nets would only die if this occurred within 10 levels.</li>
                    <li>I later changed this further so that neural nets wouldn’t die at all if they didn’t hit a block.</li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </li>
            <li>Once the first batch of neural nets die, let the second batch play and so on, until the whole generation has played.
                <ul>
                    <li>The reason the whole generation doesn’t play at once is because it’s super laggy to play 90 games at once.</li>
                </ul>
            </li>
            <li>Once every batch has played, we sort the population based on each neural nets’ fitness. In this case, the fitness is determined by the score the neural net got before dying.</li>
            <li>Once the population is sorted we
                <ul>
                    <li>Remove the bottom half.</li>
                    <li>Breed the rest so that the population is back to being 90 neural nets.</li>
                    <li>Add some random neural nets to introduce variety into the population. The hope was that this would introduce genetic diversity like in nature.</li>
                </ul>
            </li>
            <li>Let the new generation play.</li>
        </ul>

        <img class = "image image-portrait" src="../img/multiple-games.png" alt="Multiple Games">
        <div class = "image-description">Multiple games playing at once!</div>

        <h3> What Are Our Inputs? </h3>
        I originally considered finding a way to combine a block’s x and y position and value into one value, but I realized that we can input the value of each block into the neural net in order. The neural net can then learn that the first inputs are blocks towards the top of the game. Inputs toward the end are blocks towards the bottom of the game.

        <img class = "image image-landscape" src="../img/block-connections.png" alt="Block Connections">
        <div class = "image-description">Lines connecting the blocks and the input nodes show the "importance" of each block through the line's alpha.</div>

        We also want to normalize our inputs so that in later levels the neural net isn’t confused about a 77 block because it trained on block 10. We can normalize a block value by dividing by the current score. If the level is 77 and we have 77, 67, and 10, they will become 1, 0.87, and 0.13.
        <br><br>
        In addition, we want the last input to be the x position of where the neural net is shooting the ball from. We can normalize this by dividing the x position by the screen width.

        <h3> How Do We Breed Two Neural Nets? </h3>
        After tweaking the settings, I found that the best way to breed two neural nets was to
        <ul>
            <li>Select parent A and parent B.</li>
            <li>For each weight in the neural net, choose the corresponding weight randomly from either parent A or parent B.</li>
            <li>Then, randomly mutate the weights of the child by multiplying the weight by a value from 0.5 to 1.5. This allows the neural net to change, but not drastically.</li>
            <li>I also assigned the neural nets a color, so the child will have a color that’s the average of the parents’ colors.</li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>

        Before we implement all this, however, we have to make the game!

        <img class = "image image-portrait" src="../img/original-game.jpg" alt="The Original Game">
        <div class = "image-description"> The original game. </div>

        <h2> Making The Game </h2>

        I started by playing the actual game and seeing what mechanics there are.

        The game is as follows:

        <ul>
            <li>Start with one ball and one row of blocks and one collectible ball.</li>
            <li>Shoot the ball and decrement the value of any blocks that are hit.</li>
            <li>Move the old blocks down and generate a new row of blocks.</li>
            <li>Add more balls to the player’s collection if any collectible balls are hit.</li>
            <li>Repeat.</li>
        </ul>

        I measured the blocks on my phone and the block width to block height ratio was around 7:10. I created the canvas and set up a bunch of variables so that I could adjust the block width, block height, screen width, screen height, ball diameter, and much more.
        <br><br>
        Unfortunately, you can’t set the size of the canvas using a variable, so it’s hard coded anyway. :(

        <div class = "code-block">
            void setup() {
            <ul>
                <li>size(960, 728); //sWidth and sHeight, but can't use variables </li>
            </ul>
            }
        </div>

        <h3> Day 1 </h3>
        This is when I discovered that seeing how many blocks a certain shot would hit is difficult. There are multiple balls and blocks will disappear if their value reaches 0.

        <img class = "image image-portrait" src="../img/deterministic-is-hard.png" alt="Multiple Balls">
        <div class = "image-description">Multiple balls can go different ways, so it's hard to determine which blocks get hit without actually running the game.</div>

        I began to do some research into neural nets and realized that I could make my own neural net from scratch in JavaScript since the neural net consists of nodes that represent weights.

        <h3> Day 2 </h3>
        There’s a program called Processing. This allows you to write games in code that’s pretty similar to Java. I wanted to host this on a website so I looked into a JavaScript library called Processing.js which allowed you to compile Processing code into JavaScript on the fly.
        <br><br>
        I had some CORS issue so I used Firefox to run the game locally. It seemed to work when hosted on GitLab so yay! Now we can start coding the game!
        <br><br>
        I first laid out the groundwork for associating a neural net with a game and running those games in batches. I gave the neural net a <span class = "code-inline">getAngle()</span> function that returned a random number from 20 to 160, which is the range you can shoot the ball in the game.
        <br><br>
        I then had to add a ball at the beginning and use the neural net’s angle to shoot the ball.
        <br><br>
        Once I got that done I made multiple games run at once.

        <img class = "image image-portrait" src="../img/two-games.png" alt="Two Games">
        <div class = "image-description">Two games running at once. I didn't have block generation yet so they both only have one block. </div>

        <h4> Some Problems </h4>
        Fun fact: You can’t separate the Processing.js code into multiple files so this is all going into one file. Nice!
        <br><br>
        For some reason, a class towards the bottom of the file wouldn’t load in the game, and the problem was that I had a missing semicolon towards the middle so anything after that line wouldn’t load. :/
        <br><br>
        Also, at one point the console raised an error saying that the neural nets couldn’t be found, and I realized I created the games, which need the neural nets associated with them, without instantiating random neural nets beforehand. Whoops.

        <h3> Day 3 </h3>

        Now we have to generate the blocks! I played through the original game and it seems like the levels are generated somewhat like this

        <ul>
            <li>1-2 blocks for levels 1-6</li>
            <li>1-3 for 7-27</li>
            <li>2-4 for 28-42ish</li>
            <li>2-5 for 43-100</li>
            <li>And then we’ll say 5 for every level after 100 so it’s harder.</li>
        </ul>

        I used an <span class = "code-inline">if/else</span> chain but there must be a better way… Maybe some fancy dictionary or hashmap but the algorithm won’t change and it’s O(1) anyway.

        <img class = "image image-portrait" src="../img/block-generation.png" alt="Block Generation">
        <div class = "image-description">Blocks are generated!</div>

        At this point, I could see 10 games playing at the same time but because I adjusted the alpha of each game to be 255 divided by the number of games still going on, it was very faint.
        <br><br>
        As well, the block levels are shared within a generation so they have the same training set.
        <ul>
            <li>If a row exists for a level, it’s used.</li>
            <li>If not, the program generates a new one randomly using a bag of indices where blocks can be.
                <ul>
                    <li>If a block is placed, the index is removed from the random bag.</li>
                    <li>This is better than just selecting a random index from 0 to 5 because once there are only 2 spots left, the random output could be an index that’s already taken much more often.</li>
                </ul>
            </li>
        </ul>

     <!--    I added a change so that instead of updating <span class = "code-inline">TrainingGameManager</span> even if the game is already done, I put a null value in the array of <span class = "code-inline">TrainingGameManager</span>s so that we know how many games are left.

        <div class = "code-block">
            for (int i = 0; i < gameArray.length; i++) {
            <ul>
                <li>if (gameArray[i] != null) {</li>
                <li>
                    <ul>
                        <li>boolean gameDone = gameArray[i].update();</li>
                        <li>if (gameDone == true) {</li>
                        <ul>
                            <li>gameArray[i] = null;</li>
                            <li>gamesLeft--;</li>
                        </ul>
                        <li>}</li>
                    </ul>
                </li>
                <li>}</li>
            </ul>
            }
        </div> -->

        I also added a change so that when a ball is created it pulls the color from the assigned neural net rather than assigning a new random color to a game. This way we can see if a certain neural net is doing well and showing up in the next generations.

        <div class = "code-block">
            TrainingGame(NeuralNet nn) {
            <ul>
                <li>this.nn = nn;</li>
                <li>ballColor = nn.nnColor;</li>
            </ul>
            }
        </div>

        Now to add the green collectible balls. For some reason, they would only show up when they spawned and then disappear after. Apparently, I defined <span class = "code-inline">xCoord</span> and <span class = "code-inline">yCoord</span> as a class variable but then also defined a local version of each variable in the constructor, so the information would be lost after the first row.
        <br><br>
        Pro tip: Putting the code into the original Processing environment helped me find bugs really easily.
        <br><br>
        The game was now complete! The random AI would get to around level 16 or 18.

        <h3> Day 4 </h3>
        I implemented the neural net on this day.
        <br><br>
        First I was going to need some arrays for storing the input and hidden layers. Once I had those I generated random input and hidden weights from -10 to 10.
        <br><br>
        We’ll also need an activation function so we’ll just use the sigmoid function because I don’t know what else to use. After running some training I found that a longer and wider sigmoid function produced a wider range of results and yielded better performers.

        <img class = "image image-landscape" src="../img/sigmoid-function.png" alt="Sigmoid Function">
        <div class = "image-description">The sigmoid function. We’ll run the sums we get from the input layer and the hidden layer through this to constrain the output from 0 to 1. Thanks Wikipedia.</div>

        Now that the neural nets were using inputs from the game, I had to sort them based on how they performed.

        Processing.js didn’t support sorting an <span class = "code-inline"> ArrayList</span> of neural nets by a specific parameter so I had to convert the neural net <span class = "code-inline"> ArrayList</span> to a JavaScript <span class = "code-inline"> array</span>, sort it, and then convert it back to a Processing.js <span class = "code-inline"> ArrayList</span>.

        <div class = "code-block">
            // sort neural nets in descending order based on maxScore <br>
            var jsNeuralNets = []; <br><br>

            // convert to JavaScript array <br>
            for (int i = 0; i < neuralNets.size(); i++) {
            <ul>
                <li>jsNeuralNets.push(neuralNets.get(i));</li>
                <li></li>
                <li></li>
            </ul>
            } <br><br>
            jsNeuralNets.sort(function(a, b) {
            <ul>
                <li>int result = 0;</li>
                <li>float temp = b.maxScore - a.maxScore;</li>
                <li>
                    if (temp < 0) {
                    <ul>
                        <li>result = -1;</li>
                    </ul>
                    } else if (temp > 0) {
                    <ul>
                        <li>result = 1;</li>
                    </ul>
                    } else {
                    <ul>
                        <li>result = 0;</li>
                    </ul>
                    } <br>
                    return result;
                </li>
            </ul>
            }); <br><br>

            // convert back to Processing array list <br>
            for (int i = 0; i < neuralNets.size(); i++) {
            <ul>
                <li>neuralNets.set(i, jsNeuralNets[i]);</li>
            </ul>
            }
        </div>

        When I tested it out a neural net got to level 55! I didn’t even implement breeding yet. This was just a neural net with random weights.

        <img class = "image image-portrait" src="../img/the-prodigy.png" alt="The Prodigy">
        <div class = "image-description">The absolute legend! Clearly way better than the random AI!</div>

        After this, I added breeding. It was relatively simple to pick random weights from two parent neural nets. I wasn’t sure what the best method was to combine two neural nets, so I chose random weights from each parent and mutated both the child and parents. Towards the end, I decided not to mutate the parents as this might make the parents perform worse.
        <br><br>
        When I watched the neural nets play, I noticed that some neural nets would die because of the previous rule I implemented where if a neural net didn’t hit a block, it would die. However, a neural net could be well into the 40s and die, so I decided to change the rule so that only neural nets that missed before level 10 would die. This would force the neural nets to learn to hit blocks, but not punish them when they were further into the game.

        <img class = "image image-landscape" src="../img/settings.png" alt="Settings">
        <div class = "image-description"> Variables I used to try to find the ideal configuration for the neural net.</div>

        <h3> Day 5 </h3>
        I added a way to see which games are playing. I also noticed that the number of balls was way too high for the score. The number of balls is the score, and I noticed at level 9 there were around 18 balls, which is wrong. I realized that when the ball collectible reaches the bottom of the ground, I forget to remove it from the pool of balls, so it just moves down forever and keeps adding a ball each turn.

        <h3> Day 6 </h3>
        At this point the only thing to improve was how the neural nets learned and trained, so I tried letting them train on a game where all the rows were either 4 or 5 blocks in an attempt to force the neural nets to learn a better strategy than just hitting the lowest one. It didn’t really work :(
        <br><br>
        I tried removing the sigmoid function and logged the range of outputs that the neural nets would return, and it seemed to be around -1500 to 1500 max so I tried scaling it down to 1200 so the output would be 0 to 1 for the angle. I noticed it prioritizing blocks higher up to hit lots of blocks at once while leaving lower ones untouched. However, this meant that it died because it didn’t break the blocks that would end the game.
        <br><br>
        In addition, I added a way to keep track of the number of games completed and the sum of all the scores so I could display the average score.

        <img class = "image image-landscape" src="../img/average-score.png" alt="Average Score">
        <div class = "image-description"> I added a way to see the average score. Hopefully it keeps increasing! </div>

        I noticed that the neural nets keep aiming for the lowest blocks when using the sigmoid function so I tried to force it to use an “optimal” strategy where you always bounce off the top of the screen to hopefully hit more blocks.
        <br><br>
        However, killing off the neural nets if they didn’t hit the top of the screen each turn just meant the neural nets would shoot basically straight up and hope for the best.
        <br><br>
        While trying out multiple configurations I also added the ability to press space to hide the visualizations as that used less CPU (95% to 25%).

        <h3> Day 7 </h3>
        I added a visualization of the neural net but something seemed wrong about how I drew it. It was super laggy so I added a hotkey to toggle it on and off.

        <img class = "image image-landscape" src="../img/neural-net-3.png" alt="Neural Net">
        <div class = "image-description">The final version of the neural net visualization. This shows the neural net of the top performer. It looks a lot better now that the neural net has the proper weights.</div>
        <img class = "image image-portrait" src="../img/neural-net-4.png" alt="Neural Net">
        <div class = "image-description">A closer look at the neural net. Notice how the connections to the lower input nodes (inputs from blocks closer to the bottom) seem denser.</div>

        The lines going from blocks are just black lines with an alpha based on the block’s value. The lines going from the first nodes to the next ones are red or blue depending on if they’re positive or negative, and they have an alpha based on the weight divided by the max weight range. It’s the same for the lines between the hidden and output layer.
        <br><br>
        I also added a filter to draw lines only if they were above a certain threshold or else it was too laggy to draw.
        <br><br>
        After that, I tried making the sigmoid function… longer so that I would have a more distributed range of angle outputs… hopefully.
        <br><br>
        It seemed to get a higher score faster but it stopped around 87.

        <h3> Day 8 </h3>
        But it got to <strong>100</strong>!!!!!!!!!!

        <img class = "image image-landscape" src="../img/100.png" alt="100!!!">
        <div class = "image-description">YAY</div>

        To get to 100 I stopped mutating the parents so that the “good” genes would be preserved. I also didn’t exclude the top performer because I realized after each generation, levels are random again so it might not perform well again.
        <br><br>
        Most importantly, I found a <a class = "link" href="https://stackoverflow.com/a/31732257" target="_blank">post in Stack Overflow</a> that suggested mutating weights by multiplying them by a float from 0.5 to 1.5. This would allow the neural net to act basically the same but increase or decrease the emphasis placed on a certain input.
        <br><br>
        I left this neural net running and it got to 100 after 21 generations or so.
        <br><br>
        I also realized I left in a line that copied the top performer so the pool might have been just saturated with the top performer and mutations of it, but I got rid of it so that there would hopefully be variety added by the random new neural nets.

        <h3> Day 9 </h3>
        My sister left the game running and she got to <strong>103</strong>!
        <br><br>
        I noticed that all the balls tended to become white in the later generations, and it turns out that when I was calculating the average of two RGB colors I forgot to divide by 2. According to the internet, you take the square root of the average of the squares of each RGB component, but I left out the average part so I was just adding the squares together, leading to higher and higher values that eventually became (255, 255, 255).
        <br><br>
        I let the neural nets train on my computer and they got to <strong>111</strong>!

        <img class = "image image-landscape" src="../img/111.png" alt="111!">
        <div class = "image-description">YAYYYY</div>

        <h3> Day 10 </h3>
        I realized I didn’t have a unique weight for each input layer node to hidden layer node connection. The way I set it up meant that each hidden node would receive the same input.
        <br><br>
        It’s interesting that the “neural net” still did so well…
        <br><br>
        I fixed it and it seems to improve faster.

        <h2> Conclusion </h2>
        I would say this was definitely a success. The AIs that shot randomly got to level 20 at most, so getting to 111 is very good.
        <br><br>
        I was hoping that a neural net would find the optimal strategy and be able to play forever, but sadly that hasn’t happened yet.
        <br><br>
        I learned that neural nets aren't as complicated as I thought they were - the main hurdle was recreating the game in Processing. It would be cool to learn how to implement backpropagation but there isn't exactly an "expected" output for each scenario, so I'm not sure how backpropagation would work here.
        <br><br>
        If you made it this far, thanks for reading. I hope you thought this was as cool as I did!

        <!-- <span class = "code-inline">
        -->
        <!-- <div class = "code-block">
        -->
        <!-- <a class = "link" href="" target="_blank">
        -->
        <!-- <img class = "image image-portrait" src="../img/" alt="">
        <div class = "image-description"></div>
        -->
        <!-- <img class = "image image-landscape" src="../img/" alt="">
        <div class = "image-description"></div>
        -->
        <!-- <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul> -->
    </div>

    <div id = "name">
        Justin Li 2019
    </div>
</body>
</html>
