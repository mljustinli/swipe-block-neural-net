/*
 * An attempt to create a neural net that will play swipe brick breaker.
 *
 * Justin Li 5/24/19
 */

/*
 * Screen size variables
 */
int scale = 8;
int blockWidth = 10, blockHeight = 7;
blockWidth *= scale;
blockHeight *= scale;
int numBlocksWide = 6, numBlocksTall = 9;
int topBotPadding = 2;
int topBotPaddingScaled = topBotPadding * blockHeight;

int sWidth = blockWidth * numBlocksWide;
int sHeight = blockHeight * numBlocksTall + 2 * topBotPadding * blockHeight;

/*
 * Game variables
 */

//modes
int TRAINING_MODE = 0;
int PLAYING_MODE = 1; //letting the strongest neural net play a game

int mode = TRAINING_MODE;

//gen and batch stuff
int gen = 0;
int batch = 0;

int BATCHES_PER_GEN = 3; //number of batches
int NN_PER_BATCH = 30; //number of neural nets per batch

TrainingGameManager[] batchArray = new TrainingGameManager[BATCHES_PER_GEN];
ArrayList neuralNets = new ArrayList(NN_PER_BATCH * BATCHES_PER_GEN); // holds all neural nets

ArrayList sharedLevels = new ArrayList();

int topNNID = -1;
int globalBest = 0;
int sumOfAllScores = 0;
int gamesCompleted = 0;

ArrayList nnPlaying = new ArrayList();

NeuralNet nnToShow;

/*
 * Settings
 */

boolean SHOW_GRAPHICS = true;
boolean SHOW_NET = true;

boolean KILL_IF_MISS = false; //kills if it misses before 10 turns
boolean USE_SIGMOID = true;
boolean KILL_IF_NO_HIT_TOP = false;
boolean USE_TOP = true;

void setup() {
  console.log(sWidth + " " + sHeight);
  size(960, 728); //sWidth and sHeight, but can't dynamically set :(
  background(255);
  rectMode(CENTER);
  ellipseMode(CENTER);

  //set up generations, batches, and games
  for (int i = 0; i < NN_PER_BATCH * BATCHES_PER_GEN; i++) {
    neuralNets.add(new NeuralNet(NUM_INPUT, NUM_HIDDEN, NUM_OUTPUT));
  }

  createBatchArray();
  createSharedLevels();

  colorMode(RGB, 255);
}

void keyPressed() {
  if (key == 110) {
    SHOW_NET = !SHOW_NET;
  }

  if (keyCode == 32) {
    SHOW_GRAPHICS = !SHOW_GRAPHICS;
  }
}

void draw() {
  background(255);
  if (mode == TRAINING_MODE) {

    drawText();
    drawTopAndBotLines();
    drawNeuralNet();

    boolean done = batchArray[batch].update();

    if (done) {
      batch++;

      if (batch >= BATCHES_PER_GEN) {
        //evolve
        NeuralNet.evolve(neuralNets);

        //make new training game managers with new neural nets
        createBatchArray();

        //reset batch count
        batch = 0;

        //increment gen count
        gen++;

        //remake the levels
        createSharedLevels();
      }
    }
  }
}

void drawText() {
  fill(0);
  textSize(30);
  textAlign(LEFT);
  text("Gen: " + gen, 15, 30);
  text("Batch: " + batch, 15, 60);
  text("Global Best: " + globalBest, 200, 30);
  text("ID of Best: " + topNNID, 230, 60);

  fill(neuralNets.get(0).nnColor);
  rect(215, 50, 20, 20);
  fill(0);

  String currPlayingString = "";
  for (int i = 0; i < nnPlaying.size(); i++) {
    currPlayingString += nnPlaying.get(i).id + " (" + nnPlaying.get(i).parent1id + ", " + nnPlaying.get(i).parent2id + ")";
    if (i != nnPlaying.size() - 1) {
      currPlayingString += ", ";
    }
  }
  textSize(15);
  text("Currently Playing: " + currPlayingString, 15, 90);
  text("Average Score: " + ((float) sumOfAllScores) / gamesCompleted, 15, 105);
  textSize(30);

  textSize(15);
  text("Press space to hide/show graphics (for longer sessions).", sWidth, 30);
  text("Press n to hide/show the neural net.", sWidth, 60);
  text("Runs much smoother when neural net visualization is off (and all visuals... of course).", 15, sHeight - topBotPaddingScaled + 30);
  text("Numbers in parentheses represent the parents of a neural net.", 15, sHeight - topBotPaddingScaled + 60);
  text("*** Click in the game if key presses aren't working.", 15, sHeight - topBotPaddingScaled + 90);
  textSize(30);
}

void drawTopAndBotLines() {
  fill(0);
  stroke(0);
  strokeWeight(5);
  line(0, topBotPaddingScaled, sWidth, topBotPaddingScaled);
  line(0, sHeight - topBotPaddingScaled, sWidth, sHeight - topBotPaddingScaled);
}

int NODE_PADDING_TOP = topBotPaddingScaled + 10;
int NODE_PADDING_LEFT = sWidth + 30;
int NODE_SPACING_VERTICAL = 10;
int NODE_SPACING_HORIZONTAL = 200;
int NODE_DIAMETER = 3;

float DRAW_THRESHOLD = 0.8;

//draws the nodes and lines from a neural net
void drawNeuralNet() {
  if (SHOW_NET) {
    nnToShow = null;
    TrainingGameManager tgm = batchArray[batch];
    for (int i = 0; i < tgm.gameArray.length; i++) {
      if (tgm.gameArray[i] != null) {
        nnToShow = tgm.gameArray[i].nn;
        break;
      }
    }

    strokeWeight(1);

    if (SHOW_GRAPHICS && nnToShow != null) {
      float[] inputs = nnToShow.input;
      float[][] hiddenWeights = nnToShow.hiddenWeights;
      float[][] outputWeights = nnToShow.outputWeights;

      if (inputs != null) {
        for (int i = 0; i < inputs.length; i++) {
          int row = int(i / numBlocksWide) + 1;
          int col = i % numBlocksWide;

          int targetX = NODE_PADDING_LEFT;
          int targetY = NODE_PADDING_TOP + NODE_SPACING_VERTICAL * i;

          stroke(0, 255.0 * inputs[i]);

          if (i != inputs.length - 1) {
            line (col * blockWidth + blockWidth / 2, row * blockHeight + blockHeight / 2 + topBotPaddingScaled, targetX, targetY);
          } else {
            line (sWidth * inputs[inputs.length - 1], sHeight - topBotPaddingScaled, targetX, targetY);
          }

          ellipse(targetX, targetY, NODE_DIAMETER, NODE_DIAMETER);
        }
      }

      for (int i = 0; i < hiddenWeights.length; i++) {
        int targetX = NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL;
        int targetY = NODE_PADDING_TOP + NODE_SPACING_VERTICAL * i;
        for (int j = 0; j < hiddenWeights[i].length; j++) {
          int startX = NODE_PADDING_LEFT;
          int startY = NODE_PADDING_TOP + NODE_SPACING_VERTICAL * j;

          float hw = hiddenWeights[i][j];
          color c = hw > 0 ? color(144, 180, 237) : color(255, 84, 84);

          float alpha = hw / RANDOM_WEIGHT_RANGE;
          if (abs(alpha) > DRAW_THRESHOLD) {
            alpha += 1;
            alpha /= 2;
            alpha *= 255;
            stroke(c, alpha);
            line (startX, startY, targetX, targetY);
          }
        }

        float alpha = outputWeights[0][i] / RANDOM_WEIGHT_RANGE;

        if (abs(alpha) > DRAW_THRESHOLD/2) {
          alpha += 1;
          alpha /= 2;
          alpha *= 255;
          stroke(c, alpha);
          line(targetX, targetY, NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL * 2, NODE_PADDING_TOP);

          stroke(0);
        }
        ellipse(NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL, NODE_PADDING_TOP + NODE_SPACING_VERTICAL * i, NODE_DIAMETER, NODE_DIAMETER);
      }

      stroke(0);
      ellipse(NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL * 2, NODE_PADDING_TOP, NODE_DIAMETER, NODE_DIAMETER);



      // for (int i = 0; i < hiddenWeights.length; i++) {
      //   int targetX = NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL;
      //   int targetY = NODE_PADDING_TOP + NODE_SPACING_VERTICAL * i;
      //   for (int j = 0; j < inputWeights.length - 1; j++) {
      //     int startX = NODE_PADDING_LEFT;
      //     int startY = NODE_PADDING_TOP + NODE_SPACING_VERTICAL * j;

      //     if (abs(inputs[j]) > 0) {
      //       color c = inputWeights[j] > 0 ? color(144, 180, 237) : color(255, 84, 84);

      //       float alpha = inputWeights[j] / RANDOM_WEIGHT_RANGE;
      //       alpha += 1;
      //       alpha /= 2;
      //       alpha *= 255;
      //       stroke(c, alpha);
      //       line (startX, startY, targetX, targetY);
      //     }
      //   }

      //   float alpha = hiddenWeights[i] / RANDOM_WEIGHT_RANGE;
      //   alpha += 1;
      //   alpha /= 2;
      //   alpha *= 255;
      //   stroke(c, alpha);
      //   line(targetX, targetY, NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL * 2, NODE_PADDING_TOP);

      //   stroke(0);
      //   ellipse(NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL, NODE_PADDING_TOP + NODE_SPACING_VERTICAL * i, NODE_DIAMETER, NODE_DIAMETER);
      // }

      // stroke(0);
      // ellipse(NODE_PADDING_LEFT + NODE_SPACING_HORIZONTAL * 2, NODE_PADDING_TOP, NODE_DIAMETER, NODE_DIAMETER);
    }
  }
}

//creates a new set of games
void createBatchArray() {
  for (int i = 0; i < batchArray.length; i++) {
    batchArray[i] = new TrainingGameManager(NN_PER_BATCH, i);
  }
}

//resets the shared levels (usually after a new generation starts)
void createSharedLevels() {
  sharedLevels = new ArrayList();
  sharedLevels.add(new int[numBlocksWide]); //level 0 has nothing and is all false
  // ^0 for empty, 1 for block, 2 for collectible ball
}

/*
 * Classes
 */

/*
 * Represents a collection of training games.
 */
class TrainingGameManager {
  TrainingGame[] gameArray;

  int batchNum;

  int gamesLeft = NN_PER_BATCH;

  TrainingGameManager(int numGames, int batchNum) {
    this.batchNum = batchNum;
    gameArray = new TrainingGame[numGames];
    for (int i = 0; i < gameArray.length; i++) {
      gameArray[i] = new TrainingGame((NeuralNet) neuralNets.get(batchNum * NN_PER_BATCH + i));
      console.log("Weights for nn: " + gameArray[i].nn.id);
      console.log(gameArray[i].nn.inputWeights);
      console.log(gameArray[i].nn.hiddenWeights);
    }
  }

  boolean update() {
    nnPlaying = new ArrayList();
    for (int i = 0; i < gameArray.length; i++) {
      if (gameArray[i] != null) {
        boolean gameDone = gameArray[i].update();
        if (gameDone == true) {
          gameArray[i] = null;
          gamesLeft--;
        } else {
          NeuralNet n = gameArray[i].nn;
          nnPlaying.add(n);
        }
      }
    }

    ALPHA = 255.0/gamesLeft;
    return gamesLeft <= 0;
  }
}

int MIN_ANGLE = 20; //degrees limit of how low you can shoot the ball
float ALPHA = 255.0/NN_PER_BATCH;

int MILLIS_BETWEEN_BALLS = 50;

int BOUNDARY_FOR_NOT_DYING = 10; //won't die if it doesn't hit anything after this row

/*
 * Represents a training game (a single neural net plays with a seeded
 * random level)
 */
class TrainingGame {
  NeuralNet nn;

  ArrayList balls = new ArrayList();
  ArrayList blocks = new ArrayList();
  ArrayList collectibles = new ArrayList();

  ArrayList newBalls = new ArrayList();

  int score = 0;

  boolean done = false; // true means the nn died
  boolean doneShooting = true;

  boolean firstDoneShooting = false; //first ball is done
  boolean hitTop = !KILL_IF_NO_HIT_TOP;; //at least one ball has hit the top
  float[] newCoord;

  color ballColor;

  static ArrayList blockIndices; //stores ints 0 to [numBlocksWide]

  TrainingGame(NeuralNet nn) {
    this.nn = nn;

    ballColor = nn.nnColor;

    for (int i = 0; i < 1; i++) {
      balls.add(new Ball());
    }

    //add indices to blockIndices if it doesn't exist
    if (blockIndices == null) {
      blockIndices = new ArrayList(numBlocksWide);
      for (int i = 0; i < numBlocksWide; i++) {
        blockIndices.add(i);
      }
    }
  }

  // returns done if dead
  boolean update() {
    if (nn.id == topNNID && !USE_TOP) {
      console.log("Not using " + nn.id + " because it's the top.");
      done = true;
    }

    //draw blocks
    noStroke();
    textAlign(CENTER, CENTER);

    if (SHOW_GRAPHICS) {
      for (int i = 0; i < blocks.size(); i++) {
        Block b = blocks.get(i);
        b.draw();
      }
    }

    //draw collectibles
    if (SHOW_GRAPHICS) {
      for (int i = 0; i < collectibles.size(); i++) {
        BallCollectible bc = collectibles.get(i);
        bc.draw(i);
      }
    }

    if (doneShooting) {
      spawnBlocks(newBalls);
      for (int i = 0; i < balls.size(); i++) {
        Ball b = balls.get(i);
        int currTime = millis();
        b.shoot(currTime + (i + 1) * MILLIS_BETWEEN_BALLS, i == 0, nn, blocks, score);
        if (i != 0) {
          b.vx = balls.get(0).vx;
          b.vy = balls.get(0).vy;
        }
      }
      doneShooting = false;
      firstDoneShooting = false;
    }

    fill(ballColor, ALPHA);

    doneShooting = true;
    for (int i = 0; i < balls.size(); i++) {
      Ball b = balls.get(i);
      boolean ballDoneShooting = b.update(blocks, collectibles, newBalls);

      if (!firstDoneShooting && ballDoneShooting) {
        if (b.hits <= 0) {
          if (score < BOUNDARY_FOR_NOT_DYING && KILL_IF_MISS) {
            done = true;
          }
        }
        firstDoneShooting = true;
        newCoord = new float[] {b.x, b.y};
      } else if (ballDoneShooting) {
        b.x = newCoord[0];
        b.y = newCoord[1];
      }

      if (ballDoneShooting) { //this includes the first ball
        if (b.topHits > 0 && KILL_IF_NO_HIT_TOP) {
          hitTop = true;
        }
      }

      doneShooting = doneShooting && ballDoneShooting;
    }

    if (doneShooting) {
      if (!hitTop) {
        done = true;
      }

      hitTop = !KILL_IF_NO_HIT_TOP;

      for (int i = 0; i < newBalls.size(); i++) {
        Ball b = new Ball();
        b.x = newCoord[0];
        b.y = newCoord[1];
        b.vy = 1;
        balls.add(b);
      }
      newBalls = new ArrayList();
    }

    if (done) {
      sumOfAllScores += score;
      gamesCompleted++;
      hitTop = false;
    }

    return done;
  }

  //adds blocks and also a new ball collectible
  void spawnBlocks(ArrayList newBalls) {
    score++;
    if (score > nn.maxScore) {
      nn.maxScore = score;
      if (score > globalBest) {
        globalBest = score;
      }
    }

    boolean rowExists = score < sharedLevels.size();

    if (rowExists) {
      //pull blocks from there
      int[] row = sharedLevels.get(score);
      addBlocks(row);
    } else {
      //add new row of blocks
      int min = 1;
      int max;
      if (score <= 6) {
        max = 2;
      } else if (score <= 27) {
        max = 3;
      } else if (score <= 42) {
        min = 2;
        max = 4;
      } else if (score <= 100) {
        min = 2;
        max = 5;
      } else {
        min = 4;
        max = 5;
      }

      ArrayList indices = new ArrayList(blockIndices);

      int numBlocksToAdd = int(random(min, max + 1));
      int[] newRow = new int[numBlocksWide];
      for (int i = 0; i < numBlocksToAdd + 1; i++) {
        int indexIndex = int(random(0, indices.size())); //index in the arraylist of the number to pull out
        int indexToAdd = indices.get(indexIndex); //actual number to use
        indices.remove(indexIndex);

        if (i >= numBlocksToAdd) {
          newRow[indexToAdd] = 2;
        } else {
          newRow[indexToAdd] = 1;
        }
      }

      sharedLevels.add(newRow);
      addBlocks(newRow);
    }

    //move all current blocks down
    for (int i = 0; i < blocks.size(); i++) {
      Block b = blocks.get(i);
      boolean dead = b.moveDown();
      if (dead) {
        done = true;
      }
    }

    //move all current collectibles down
    ArrayList collectiblesToRemove = new ArrayList();

    for (int i = 0; i < collectibles.size(); i++) {
      BallCollectible bc = collectibles.get(i);
      boolean touchingGround = bc.moveDown();
      if (touchingGround && !newBalls.contains(bc.id)) {
        newBalls.add(bc.id);
        collectiblesToRemove.add(bc);
      }
    }

    for (int i = 0; i < collectiblesToRemove.size(); i++) {
      collectibles.remove(collectiblesToRemove.get(i));
    }
  }

  void addBlocks(int[] row) {
    for (int i = 0; i < row.length; i++) {
      if (row[i] == 1) {
        blocks.add(new Block(score, i));
      } else if (row[i] == 2) { //has ball collectible
        collectibles.add(new BallCollectible(i));
      }
    }
  }
}

/*
 * Represents a block.
 */

class Block {
  int value;
  int x, y;
  int xCoord, yCoord; //coords scaled up

  Block(int value, int x) {
    this.value = value;
    this.x = x;
    this.y = 0;
    updateCoords();
  }

  void draw() {
    fill(0, ALPHA);
    rect(xCoord, yCoord, blockWidth, blockHeight);

    fill(255, ALPHA);
    text(value + "", xCoord, yCoord);
  }

  void updateCoords() {
    xCoord = x * blockWidth + blockWidth/2;
    yCoord = y * blockHeight + blockHeight/2 + topBotPaddingScaled;
  }

  // returns true if it's at the bottom (game is over)
  boolean moveDown() {
    y++;
    updateCoords();
    return y >= numBlocksTall - 1;
  }
}

/*
 * Represents a ball that you can collect. If you hit it or if it reaches
 * the bottom, the number of balls the player has increases.
 */

class BallCollectible {
  static int ID = 0;
  int id;

  int x, y;
  int xCoord, yCoord;

  BallCollectible(int x) {
    ID++;
    id = ID;

    this.x = x;
    this.y = 0;
    updateCoords();
  }

  void draw(int i) {
    fill(155, 255, 155, ALPHA);
    ellipse(xCoord, yCoord, BALL_DIAMETER, BALL_DIAMETER);
  }

  void updateCoords() {
    xCoord = x * blockWidth + blockWidth/2;
    yCoord = y * blockHeight + blockHeight/2 + topBotPaddingScaled;
  }

  //returns true if it touches the ground
  boolean moveDown() {
    y++;
    updateCoords();
    return y >= numBlocksTall;
  }
}


/*
 * Represents a ball!
 */

int BALL_DIAMETER = blockWidth/4;
int BALL_SPEED = 12; //6

class Ball {
  float x, y; //scaled coordinates
  float vx, vy;

  int timeToGo;
  boolean moving = false;
  boolean done = false;

  int hits; //num of times the ball hits a block
  int topHits; //num of times the ball hits the top boundary

  Ball() {
    x = sWidth/2;
    y = sHeight - BALL_DIAMETER/2 - topBotPaddingScaled;
    timeToGo = 0;

    hits = 0;
    topHits = 0;
  }

  void draw() {
    stroke(0, ALPHA);
    strokeWeight(4);
    ellipse(x, y, BALL_DIAMETER, BALL_DIAMETER);
    noStroke();
  }

  //returns true when the ball is done moving
  boolean update(ArrayList blocks, ArrayList collectibles, ArrayList newBalls) {
    if (millis() >= timeToGo && !moving && !done) {
      moving = true;
      hits = 0;
      topHits = 0;
    }
    if (moving) {
      x += vx;
      y += vy;

      //left and right boundary
      if (x > sWidth - BALL_DIAMETER / 2) {
        vx = -1 * abs(vx);
      }

      if (x < 0 + BALL_DIAMETER / 2) {
        vx = abs(vx);
      }

      //top boundary
      if (y < topBotPaddingScaled + BALL_DIAMETER / 2) {
        topHits++;
        vy = abs(vy);
      }

      int bottom = sHeight - BALL_DIAMETER/2 - topBotPaddingScaled;
      if (vy > 0 && y >= bottom) {
        y = bottom;
        moving = false;
        done = true;
      }

      //check collision with blocks
      ArrayList blocksToRemove = new ArrayList(); //stores objects
      for (int i = 0; i < blocks.size(); i++) {
        Block b = blocks.get(i);

        boolean hitBlock = false;

        //top or bottom edge
        if (x > b.xCoord - blockWidth/2 && x < b.xCoord + blockWidth/2) {
          if (y > b.yCoord + blockHeight/2 && y < b.yCoord + blockHeight/2 + BALL_DIAMETER/2) {
            vy = abs(vy);
            hitBlock = true;
          }
          if (y < b.yCoord - blockHeight/2 && y > b.yCoord - blockHeight/2 - BALL_DIAMETER/2) {
            vy = -abs(vy);
            hitBlock = true;
          }
        }

        //left or right edge
        if (y > b.yCoord - blockHeight/2 && y < b.yCoord + blockHeight/2) {
          if (x < b.xCoord + blockWidth/2 + BALL_DIAMETER/2 && x > b.xCoord + BALL_DIAMETER/2) {
            vx = abs(vx);
            hitBlock = true;
          }
          if (x > b.xCoord - blockWidth/2 - BALL_DIAMETER/2 && x < b.xCoord - BALL_DIAMETER/2) {
            vx = -abs(vx);
            hitBlock = true;
          }
        }

        if (hitBlock) {
          b.value--;
          hits++;
        }

        if (b.value <= 0) {
          blocksToRemove.add(b);
        }
      }

      for (int i = 0; i < blocksToRemove.size(); i++) {
        blocks.remove(blocksToRemove.get(i));
      }

      //check collision with collectible balls
      ArrayList collectiblesToRemove = new ArrayList();
      for (int i = 0; i < collectibles.size(); i++) {
        BallCollectible bc = collectibles.get(i);

        float dist = sqrt(pow(bc.xCoord - x, 2) + pow(bc.yCoord - y, 2));
        if (dist <= BALL_DIAMETER && !newBalls.contains(bc.id)) {
          newBalls.add(bc.id);
          collectiblesToRemove.add(bc);
        }
      }

      for (int i = 0; i < collectiblesToRemove.size(); i++) {
        collectibles.remove(collectiblesToRemove.get(i));
      }
    }

    if (SHOW_GRAPHICS) {
      draw();
    }
    return done;
  }

  void shoot(int timeToGo, boolean first, NeuralNet nn, ArrayList blocks, int score) {
    if (first) {
      float angle = nn.getAngle(blocks, score, x); //input will change
      setVelocity(angle);
    }

    this.timeToGo = timeToGo;
    done = false;
  }

  void setVelocity(float angle) {
    vx = cos(angle);
    vy = -1 * sin(angle);

    float length = sqrt(pow(vx, 2) + pow(vy, 2)) / BALL_SPEED;
    vx /= length;
    vy /= length;
  }
}

/*
 * Represents a neural net.
 */

int NUM_INPUT = numBlocksWide * (numBlocksTall - 1) + 1;
int NUM_HIDDEN = int((2.0 / 3) * NUM_INPUT);
int NUM_OUTPUT = 1;

int RANDOM_WEIGHT_RANGE = 10; // 10

float KEEP_PROPORTION = 1.0 / 3;

float MATH_E = Math.exp(1);

float CHANCE_OF_MUTATION = 0.10; // 0.05
float CHANCE_OF_ADDING_RANDOM_NN = 0.2; // 0.2

int SIGMOID_FACTOR = 10;

int MAX_OUTPUT_VALUE = 1000; //1000 worked well

class NeuralNet implements Comparable<NeuralNet> {
  static int ID = 1;
  int id;

  int parent1id;
  int parent2id;

  int maxScore;

  int numInput, numHidden, numOutput;

  float[][] hiddenWeights;
  float[][] outputWeights;

  color nnColor;

  float[] input; //save for drawing

  NeuralNet(int numInput, int numHidden, int numOutput) {
    id = ID;
    ID++;

    // parent1id = -1;
    // parent2id = -1;

    maxScore = 0;

    this.numInput = numInput;
    this.numHidden = numHidden;
    this.numOutput = numOutput;

    hiddenWeights = new float[numHidden][numInput];
    outputWeights = new float[numOutput][numHidden];


    fillWeights(hiddenWeights);
    fillWeights(outputWeights);

    nnColor = color(random(255), random(255), random(255));
  }

  void fillWeights(float[][] arr) {
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        arr[i][j] = random(-RANDOM_WEIGHT_RANGE, RANDOM_WEIGHT_RANGE);
      }
    }
  }

  float getAngle(ArrayList blocks, int score, float ballX) {
    input = generateInputFromBlockArrayList(blocks, score, ballX);

    // get array of inputs for hidden layer

    // calculate array of h1, h2, h3 ...

    // use new array to calculate array of o1, o2 ...

    float[] hiddenLayerResults = new float[numHidden];
    for (int i = 0; i < hiddenWeights.length; i++) {
      float sum = 0;
      for (int j = 0; j < hiddenWeights[i].length; j++) {
        sum += input[j] * hiddenWeights[i][j];
      }

      if (USE_SIGMOID) {
        hiddenLayerResults[i] = 1 / (1 + pow(MATH_E, -sum / SIGMOID_FACTOR)); //apply sigmoid to sum of weighted inputs
      } else {
        hiddenLayerResults[i] = sum;
      }
    }

    float[] outputLayerResults = new float[numOutput];
    for (int i = 0; i < outputWeights.length; i++) {
      float sum = 0;
      for (int j = 0; j < outputWeights[i].length; j++) {
        sum += hiddenLayerResults[j] * outputWeights[i][j];
      }

      if (USE_SIGMOID) {
        outputLayerResults[i] = 1 / (1 + pow(MATH_E, -sum / SIGMOID_FACTOR)); //apply sigmoid to sum of weighted inputs
      } else {
        outputLayerResults[i] = sum;
      }
    }

    float angle;
    if (USE_SIGMOID) {
      angle = MIN_ANGLE + (180 - 2 * MIN_ANGLE) * outputLayerResults[0];
    } else {
      angle = outputLayerResults[0] / MAX_OUTPUT_VALUE;
      if (angle > 1) {
        angle = 1;
      } else if (angle < -1) {
        angle = -1;
      }
      angle += 1;
      angle /= 2; //change to 0 to 1 instead of -1 to 1

      angle = MIN_ANGLE + (180 - 2 * MIN_ANGLE) * angle;
    }

    //temporary behavior for testing
    // float angle = random(MIN_ANGLE, 180 - MIN_ANGLE);
    return radians(angle);
  }

  float[] generateInputFromBlockArrayList(ArrayList blocks, int score, float ballX) {
    float[] input = new float[NUM_INPUT];

    float newBallX = ballX / sWidth;
    input[NUM_INPUT - 1] = newBallX;

    for (int i = 0; i < blocks.size(); i++) {
      Block b = blocks.get(i);

      float val = b.value / score;
      input[numBlocksWide * (b.y - 1) + b.x] = val;
    }

    return input;
  }

  int compareTo(NeuralNet o) {
    NeuralNet e = (NeuralNet) o;
    return maxScore - e.maxScore;
  }

  static void evolve(ArrayList neuralNets) {
    int numNeuralNets = neuralNets.size();

    //sort the neural nets in descending order based on how they perform (maxScore)
    var jsNeuralNets = [];
    for (int i = 0; i < neuralNets.size(); i++) {
      jsNeuralNets.push(neuralNets.get(i));
    }
    jsNeuralNets.sort(function(a, b){
      int result = 0;
      float temp = b.maxScore - a.maxScore;
      if (temp < 0){
       result = -1;
     } else if (temp > 0){
       result = 1;
     } else {
       result = 0;
     }
     return result;
    });

    for (int i = 0; i < neuralNets.size(); i++) {
      neuralNets.set(i, jsNeuralNets[i]);
      // console.log("maxScore: " + neuralNets.get(i).maxScore + " " + neuralNets.get(i).id);
    }

    topNNID = neuralNets.get(0).id;
    console.log("Top is: " + topNNID);

    //now to evolve them

    //remove bottom half
    int numToRemove = int(neuralNets.size() * (1.0 - KEEP_PROPORTION));
    for (int i = 0; i < numToRemove; i++) {
      neuralNets.remove(neuralNets.size() - 1);
    }

    // //add in a copy of the top performer
    // NeuralNet topCopy = new NeuralNet(NUM_INPUT, NUM_HIDDEN, NUM_OUTPUT);
    // topCopy.hiddenWeights = neuralNets.get(0).hiddenWeights;
    // topCopy.outputWeights = neuralNets.get(0).outputWeights;
    // topCopy.nnColor = neuralNets.get(0).nnColor;

    // neuralNets.add(topCopy);

    //breed the children and mutate
    while(neuralNets.size() < numNeuralNets) {
      if (random() <= CHANCE_OF_ADDING_RANDOM_NN) {
        console.log("added new random neural net");
        neuralNets.add(new NeuralNet(NUM_INPUT, NUM_HIDDEN, NUM_OUTPUT));
      } else {
        int parent1Index = int(random(numNeuralNets - numToRemove - 1));
        int parent2Index = int(random(numNeuralNets - numToRemove - 1));

        if (parent1Index != parent2Index) {
          NeuralNet parent1 = neuralNets.get(parent1Index);
          NeuralNet parent2 = neuralNets.get(parent2Index);
          neuralNets.add(parent1.breedWith(parent2));
        }
      }
    }
  }

  NeuralNet breedWith(NeuralNet other) {
    NeuralNet child = new NeuralNet(numInput, numHidden, numOutput);

    child.parent1id = id;
    child.parent2id = other.id;

    for (int i = 0; i < hiddenWeights.length; i++) {
      for (int j = 0; j < hiddenWeights[i].length; j++) {
        child.hiddenWeights[i][j] = random() <= 0.5 ? hiddenWeights[i][j] : other.hiddenWeights[i][j];

        if (random() <= CHANCE_OF_MUTATION) {
          console.log("MUTATED!!!!!");
          child.hiddenWeights[i][j] = mutate(child.hiddenWeights[i][j]);
        }
      }
    }

    for (int i = 0; i < outputWeights.length; i++) {
      for (int j = 0; j < outputWeights.length; j++) {
        child.outputWeights[i][j] = random() <= 0.5 ? outputWeights[i][j] : other.outputWeights[i][j];

        if (random() <= CHANCE_OF_MUTATION) {
          console.log("MUTATED!!!!!");
          child.outputWeights[i][j] = mutate(child.outputWeights[i][j]);
        }
      }
    }

    //average the rgb colors of the neural nets
    int redColor;
    int greenColor;
    int blueColor;

    color color1 = nnColor;
    color color2 = other.nnColor;

    redColor = int(sqrt((pow(red(color1), 2) + pow(red(color2), 2)) / 2));
    greenColor = int(sqrt((pow(green(color1), 2) + pow(green(color2), 2)) / 2));
    blueColor = int(sqrt((pow(blue(color1), 2) + pow(blue(color2), 2)) / 2));

    child.nnColor = color(redColor, greenColor, blueColor);

    return child;
  }

  float mutate(float value) {
    return random(0.5, 1.5) * value;
  }
}
