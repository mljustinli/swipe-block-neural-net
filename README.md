# Swipe Brick Breaker Neural Net

This project trains neural nets to play Swipe Brick Breaker using NEAT (NeuroEvolution of Augmenting Topologies).

The top score is 111.

Train some neural nets [here](https://mljustinli.gitlab.io/swipe-block-neural-net/).

## The Process

Check out the process of creating the project [here](https://mljustinli.gitlab.io/swipe-block-neural-net/process/).
